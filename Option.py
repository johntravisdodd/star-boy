import random


# An option that the player can choose which holds consequences and results
class Option:

    def __init__(self, text, win_text, win_vessel, chance_to_win, fail_text=None, fail_vessel=None, events=None):

        # seed random
        random.seed()

        # Determine success
        self.success = False

        if random.randrange(0, 100) < chance_to_win * 100:
            self.success = True

        # Set Variables
        self.text = text
        self.win_text = win_text
        self.win_vessel = win_vessel
        self.fail_text = fail_text
        self.fail_vessel = fail_vessel
        self.events = []

        if events is not None:
            events.extend(events)

    # Gets the ship's state to apply as a consequence of this option
    def get_ship(self):

        if self.success is True:
            return self.win_vessel
        else:
            return self.fail_vessel

    # Gets the final result of the option as text (e.g. You go around the nebula.)
    def get_final_text(self):

        if self.success is True:
            return self.win_text
        else:
            return self.fail_text

    # Returns events related to this mission, if any
    def get_mission(self):

        if len(self.events) < 1:
            return None

        if self.success is True:
            return self.events
        else:
            return None
