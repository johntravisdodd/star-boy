# Holds a list of options for the player to choose, an image path, and a description of the event
class Event:

    def __init__(self, text, image_path=''):
        self.text = text
        self.options = []
        self.image_path = image_path
