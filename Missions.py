from Event import *
from Option import *
from Vessel import *


# Generates the missions and scenarios used in the game
class Missions:

    # Returns a list of events related to the distress call mission
    @staticmethod
    def distress_call():
        events = []

        # region Distress-Call
        # CREATE EVENT
        mission = Event(
            "You receive a distress signal from deep within a mysterious nebula!\n'Please help us, our gold "
            "shipment...' - The message cuts out.")

        # Option 1
        reward = Vessel(health=0, fuel=-5, morale=0, money=0)
        take_mission = Option(
            text="Oh my goodness it sounds like their gold...I mean that ship is in trouble! Luckily no mission is "
                 "too dangerous for this crew.",
            win_text="You order your ship into the mysterious nebula...(-5 fuel)", win_vessel=reward,
            chance_to_win=1.0)
        mission.options.append(take_mission)
        # Option 2
        reward = Vessel(health=0, fuel=-1, morale=0, money=0)
        consequence = Vessel(health=0, fuel=0, morale=0, money=0)
        option = Option(text="This is obviously a trap. I'm not falling for it.",
                        win_text="You continue on your way. (-1 fuel)", win_vessel=reward, chance_to_win=1.0)
        mission.options.append(option)

        # Add Event
        events.append(mission)
        # endregion

        # region Asteroid-Field
        # Add events to mission
        # CREATE EVENT
        event = Event("Your helmsman warns you of an asteroid field ahead.", image_path="Images\\Asteroid.png")

        # Option 1
        reward = Vessel(health=0, fuel=5, morale=5, money=0)
        consequence = Vessel(health=-10, fuel=0, morale=0, money=0)
        option = Option(text="Full speed ahead. This crew is the bravest there is.",
                        win_text="Your helmsman skillfully navigates through the field. You even find some ore "
                                 "asteroids to replenish your ship's fuel. (+5 fuel, +5 morale)",
                        win_vessel=reward,
                        fail_text="Your helmsan tries to navigate through the field but isn't very good. Your "
                                  "ship takes a beating. (-10 health)",
                        fail_vessel=consequence,
                        chance_to_win=0.33)
        event.options.append(option)
        # Option 2
        reward = Vessel(health=0, fuel=-5, morale=0, money=0)
        consequence = Vessel(health=-5, fuel=-5, morale=0, money=0)
        option = Option(text="Go around the field.",
                        win_text="Smart move, but it costs you some fuel. (-5 fuel)", win_vessel=reward,
                        fail_text="An old defense satellite fires at your ship! You take 5 damage before you "
                                  "can destroy it. (-5 health, -5 fuel)",
                        fail_vessel=consequence,
                        chance_to_win=0.75)
        event.options.append(option)
        # Add Event
        take_mission.events.append(event)
        # endregion

        # region Tow-Them-Out
        event = Event("You find the ship sending the distress call. They ran out of fuel!")

        # Option 1
        reward = Vessel(health=0, fuel=0, morale=-30, money=10000)
        option = Option(text="Take all their gold and leave them to die!",
                        win_text="You take all their gold and leave them to die. The crew is upset you did "
                                 "this. (-30 morale, +$10000)", win_vessel=reward,
                        chance_to_win=1.0)
        event.options.append(option)
        # Option 2
        reward = Vessel(health=0, fuel=-20, morale=30, money=250)
        option = Option(text="Tow them to the nearest space station.",
                        win_text="They pay you $250 for the tow and the crew celebrates the rescue by eating "
                                 "ice cream sandwiches. (+$250, +30 morale, -20 fuel)",
                        win_vessel=reward, chance_to_win=1.0)
        event.options.append(option)
        # Add Event
        take_mission.events.append(event)
        # endregion

        # return our events...
        return events

    # Returns a list of events related to the ship on fire mission
    @staticmethod
    def ship_fire():
        events = []

        # region Distress-Call
        # CREATE EVENT
        mission = Event(
            "A fire alarm is going off in the galley!", "Images\\SpaceshipFire.png")

        # Option 1
        reward = Vessel(health=5, fuel=0, morale=5, money=0)
        option = Option(
            text="Battlestations! All crew to firefighting stations, seal all doors and hatches. Activate the fire "
                 "suppression system!",
            win_text="The crew immediately jumps into action to put out the fire. (+5 health, +5 morale)",
            win_vessel=reward,
            chance_to_win=1.0)
        mission.options.append(option)
        # Option 2
        reward = Vessel(health=-10, fuel=0, morale=-10, money=0)
        consequence = Vessel(health=-50000, fuel=0, morale=0, money=0)
        take_mission = Option(text="Probably just a false alarm.",
                              win_text="A few minutes go by and the chef comes running onto the bridge. 'Bilbo "
                                       "burned to death and the fire is spreading!' (-10 health, -10 morale)",
                              win_vessel=reward,
                              fail_text="The fire spreads to the engine room! Several crewmen die fighting it "
                                        "but it reaches the fuel reserves!",
                              fail_vessel=consequence,
                              chance_to_win=0.75)
        mission.options.append(take_mission)
        # Add Event
        events.append(mission)
        # endregion

        # region The-Fire-Rises
        event = Event("The fire spreads and is heading towards the ship's fuel tanks.", "Images\\SpaceshipFire.png")

        # Option 1
        reward = Vessel(health=-20, fuel=0, morale=-20, money=0)
        option = Option(text="Vent oxygen to the fire and put it out (will kill the fire crew).",
                        win_text="You seal off the effected compartments and vent all oxygen. The fire is out! "
                                 "(-20 morale, -20 health)", win_vessel=reward,
                        chance_to_win=1.0)
        event.options.append(option)
        # Option 2
        reward = Vessel(health=0, fuel=-20, morale=0, money=0)
        option = Option(text="Empty the nearby fuel tanks.",
                        win_text="You vent the fuel into space. After several hours the fire is out. (-20 fuel)",
                        win_vessel=reward,
                        chance_to_win=1.0)
        event.options.append(option)
        # Add Event
        take_mission.events.append(event)
        # endregion

        # return our events...
        return events

    # Returns a list of one-off events which are unrelated to each other
    @staticmethod
    def one_off_events():
        events = []

        # region Radiation-Leak
        # CREATE EVENT
        event = Event("There is a radiation leak on the ship!", image_path="Images\\Radiation.png")

        # Option 1
        reward = Vessel(health=10, fuel=0, morale=0, money=0)
        consequence = Vessel(health=-10, fuel=0, morale=0, money=0)
        option = Option(text="Order a crew member to seal the leak!",
                        win_text="The leak was sealed! (+10 health)", win_vessel=reward,
                        fail_text="You send the ship's janitor to fix the leak but they make it worse! (-10 "
                                  "health)",
                        fail_vessel=consequence,
                        chance_to_win=0.75)
        event.options.append(option)
        # Option 2
        reward = Vessel(health=-5, fuel=0, morale=0, money=0)
        consequence = Vessel(health=-20, fuel=0, morale=0, money=-300)
        option = Option(text="A little radiation never hurt anyone, ignore the leak!",
                        win_text="You feel a little rad sick but only on Thursdays. (-5 health)",
                        win_vessel=reward,
                        fail_text="The leak gets worse. You finally pay someone to fix it but not before "
                                  "everyone gets space cancer. (-20 health, -$300)",
                        fail_vessel=consequence,
                        chance_to_win=0.90)
        event.options.append(option)
        # Add Event
        events.append(event)
        # endregion

        # region Fire Lasers
        # CREATE EVENT
        event = Event("You get drunk and want to shoot something.", image_path="Images\\Lasers.png")

        # Option 1
        reward = Vessel(health=0, fuel=0, morale=10, money=0)
        consequence = Vessel(health=0, fuel=0, morale=0, money=-100)
        option = Option(text="Fire the ships lasers at an asteroid.",
                        win_text="You blast asteroids all day, the crew loves it! (+10 morale)",
                        win_vessel=reward,
                        fail_text="You blast some asteroids but accidently hit a satellite and are fined by the "
                                  "Federal Space Commission! (-$100)",
                        fail_vessel=consequence,
                        chance_to_win=0.35)
        event.options.append(option)
        # Option 2
        reward = Vessel(health=0, fuel=0, morale=0, money=20)
        consequence = Vessel(health=0, fuel=0, morale=-5, money=0)
        option = Option(text="Drink more.",
                        win_text="You awaken with a hangover and $20 in your pocket. (+$20)", win_vessel=reward,
                        fail_text="The crew is sick of you getting drunk all the time. (-5 morale)",
                        fail_vessel=consequence,
                        chance_to_win=0.50)
        event.options.append(option)
        # Add Event
        events.append(event)
        # endregion

        # region Nargon-Egg
        # CREATE EVENT
        event = Event("The first mate bets you $100 that she can swallow a Nargon egg whole.",
                      image_path="Images\\Egg.png")

        # Option 1
        reward = Vessel(health=0, fuel=0, morale=0, money=100)
        consequence = Vessel(health=0, fuel=0, morale=0, money=-100)
        option = Option(text="No way! (bet $100)",
                        win_text="She almost chokes to death trying to swallow the egg but it is too big. (+$100)",
                        win_vessel=reward,
                        fail_text="She swallows the egg! Wow that's incredible!! (-$100)",
                        fail_vessel=consequence,
                        chance_to_win=0.5)
        event.options.append(option)
        # Option 2
        reward = Vessel(health=0, fuel=0, morale=5, money=20)
        consequence = Vessel(health=0, fuel=0, morale=-5, money=0)
        option = Option(text="Send her to the brig.",
                        win_text="The first mate is sent to the brig for a day. The crew thought she was being "
                                 "annoying also. (+5 morale)",
                        win_vessel=reward,
                        fail_text="The first mate is sent to the brig for a day. The crew whispers angrily "
                                  "about you. (-5 morale)",
                        fail_vessel=consequence,
                        chance_to_win=0.10)
        event.options.append(option)
        # Option 3
        reward = Vessel(health=0, fuel=0, morale=1, money=-5)
        consequence = Vessel(health=0, fuel=0, morale=-1, money=-5)
        option = Option(text="Pay her $5 to make you an omelette.",
                        win_text="She makes you a really good Nargon omelette. (+1 morale, -$5)",
                        win_vessel=reward,
                        fail_text="She undercooks the omelette but you foolishly eat it anyway and get sick. ("
                                  "-1 morale, -$5)",
                        fail_vessel=consequence,
                        chance_to_win=0.70)
        event.options.append(option)
        # Add Event
        events.append(event)
        # endregion

        # region Weird-Truck
        # CREATE EVENT
        event = Event("You find a strange truck from the 1930's floating in space.",
                      image_path="Images\\SpaceTruck.png")

        # Option 1
        reward = Vessel(health=0, fuel=10, morale=0, money=0)
        consequence = Vessel(health=0, fuel=-3, morale=0, money=0)
        option = Option(text="Bring the truck onboard!",
                        win_text="It still had fuel in its gas tank, what a haul! (+10 fuel)", win_vessel=reward,
                        fail_text="You try to bring it onboard but your crew is too incompetent to do so. (-3 "
                                  "fuel)",
                        fail_vessel=consequence,
                        chance_to_win=0.6)
        event.options.append(option)
        # Option 2
        reward = Vessel(health=20, fuel=0, morale=5, money=0)
        consequence = Vessel(health=0, fuel=-15, morale=0, money=0)
        option = Option(text="Investigate the source of the space truck.",
                        win_text="You find out that Amelia Earhart was abducted long ago, but she is in "
                                 "cryo-sleep. You wake her up and she joins your crew. (+20 health, +5 morale)",
                        win_vessel=reward,
                        fail_text="You find the corpse of Amelia Earhart in a cryo-tube. This was a "
                                  "waste of time. (-15 fuel, -3 morale)",
                        fail_vessel=consequence,
                        chance_to_win=0.55)
        event.options.append(option)
        # Add Event
        events.append(event)
        # endregion

        # region MLM Scheme
        # CREATE EVENT
        event = Event("You receive a space advertisement about a reverse funnel scheme. If you get 5 friends to "
                      "sign up you can make $1000!", image_path="Images\\Pyramid.png")

        # Option 1
        reward = Vessel(health=0, fuel=0, morale=0, money=1000)
        consequence = Vessel(health=0, fuel=0, morale=0, money=-500)
        option = Option(text="Where do I sign?",
                        win_text="You got in on the ground floor and made $1000!", win_vessel=reward,
                        fail_text="You sign up but your friends refuse to join even though you already bought 2 "
                                  "tons of powdered milk shakes. (-$500)",
                        fail_vessel=consequence,
                        chance_to_win=0.6)
        event.options.append(option)
        # Option 2
        reward = Vessel(health=0, fuel=0, morale=0, money=0)
        option = Option(text="This is just a pyramid scheme! I'm not signing!",
                        win_text="Nothing happens.",
                        win_vessel=reward,
                        chance_to_win=1.0)
        event.options.append(option)
        # Add Event
        events.append(event)
        # endregion

        # region Space-Race
        # CREATE EVENT
        event = Event("While waiting at a space stoplight you are challenged to a race around the nearest star "
                      "system!", image_path="Images\\Race.png")

        # Option 1
        reward = Vessel(health=0, fuel=50, morale=0, money=500)
        consequence = Vessel(health=0, fuel=0, morale=0, money=-200)
        option = Option(text="Accept the race. You once made the Kessel run in 12 Parsecs",
                        win_text="You win the race! You take the other ships fuel and $500 as a prize. (+$500, "
                                 "+50 fuel)", win_vessel=reward,
                        fail_text="You lose the race! The other ship takes $200 as a prize. (-$200)",
                        fail_vessel=consequence,
                        chance_to_win=0.75)
        event.options.append(option)
        # Option 2
        reward = Vessel(health=0, fuel=0, morale=5, money=0)
        option = Option(text="Refuse to race.",
                        win_text="You refuse to race but talk about how you would have won the race for the "
                                 "next 3 weeks. (+5 morale)",
                        win_vessel=reward,
                        chance_to_win=1.0)
        event.options.append(option)
        # Add Event
        events.append(event)
        # endregion

        # region The-Red-Wire
        # CREATE EVENT
        event = Event("The ships engine needs to be repaired but the engineer is sick. He tells you how to "
                      "open the engine but then passes out from exhaustion.", image_path="Images\\Wire.png")

        # Option 1
        reward = Vessel(health=0, fuel=10, morale=10, money=0)
        option = Option(text="Cut the red wire.",
                        win_text="You cut the red wire. No one knows how but that fixed it and made the ship's "
                                 "engines more efficient. (+10 fuel, +10 morale)", win_vessel=reward,
                        chance_to_win=1.0)
        event.options.append(option)
        # Option 2
        reward = Vessel(health=0, fuel=-5, morale=0, money=100)
        option = Option(text="Cut the purple wire.",
                        win_text="That didn't fix it, but you find $100 was hiding behind the purple wire! (-5 "
                                 "fuel, +$100)", win_vessel=reward, chance_to_win=1.0)
        event.options.append(option)
        # Option 3
        reward = Vessel(health=-1000, fuel=0, morale=0, money=0)
        option = Option(text="Reroute all power from life-support!",
                        win_text="That definitely kills everyone. Why would that ever work??", win_vessel=reward,
                        chance_to_win=1.0)
        event.options.append(option)
        # Option 4
        reward = Vessel(health=0, fuel=15, morale=0, money=-100)
        consequence = Vessel(health=0, fuel=-5, morale=0, money=-100)
        option = Option(text="Place a $100 bill behind the purple wire.",
                        win_text="That somehow worked. (+15 fuel, -$100)",
                        win_vessel=reward,
                        fail_text="That has worked at least once in the past, but not this time. (-$100, -5 fuel)",
                        fail_vessel=consequence, chance_to_win=0.5)
        event.options.append(option)

        # Add Event
        events.append(event)
        # endregion

        # Add a gas station event.
        events.append(Missions.gas_station_event())

        # Add some dilemas
        events.extend(Missions.dilemmas())

        return events

    # Returns the gas station event.
    @staticmethod
    def gas_station_event():
        # region Buy-Stuff
        # CREATE EVENT
        event = Event("You stop at a strange diner/space station called Deep Space Dine.",
                      image_path="Images\\DS11.png")

        # Option 1
        reward = Vessel(health=0, fuel=20, morale=0, money=-100)
        option = Option(text="Buy 20 fuel for $100.",
                        win_text="You buy fuel.", win_vessel=reward,
                        chance_to_win=1.0)
        event.options.append(option)
        # Option 2
        reward = Vessel(health=50, fuel=0, morale=0, money=-200)
        option = Option(text="Buy health potions for the crew for $200.",
                        win_text="You buy health potions for your crew. (+50 health)", win_vessel=reward,
                        chance_to_win=1.0)
        event.options.append(option)
        # Option 3
        reward = Vessel(health=0, fuel=0, morale=30, money=100)
        option = Option(text="Buy everyone their preferred snack for $100.",
                        win_text="Everyone is happy now. (+30 morale)", win_vessel=reward,
                        chance_to_win=1.0)
        event.options.append(option)
        # Option 4
        reward = Vessel(health=0, fuel=0, morale=0, money=1000)
        consequence = Vessel(health=0, fuel=0, morale=-5, money=-100)
        option = Option(text="Buy $100 in lotto tickets.",
                        win_text="You won $1000!",
                        win_vessel=reward,
                        fail_text="You didn't win anything. (-5 morale)",
                        fail_vessel=consequence, chance_to_win=0.1)
        event.options.append(option)

        # Add Event
        return event
        # endregion

    # Returns several straight-forward dileman events (e.g. fuel vs morale, etc.)
    @staticmethod
    def dilemmas():
        events = []

        # region Black-Hole
        event = Event("A black hole is in your path! What do you do?", image_path="Images\\BlackHole.png")

        # Option 1
        reward = Vessel(health=0, fuel=-10, morale=0, money=0)
        option = Option(text="Fly around it.",
                        win_text="You fly all the way around it. (-10 fuel)", win_vessel=reward,
                        chance_to_win=1.0)
        event.options.append(option)
        # Option 2
        reward = Vessel(health=-1000, fuel=0, morale=0, money=0)
        option = Option(text="Go through it!",
                        win_text="This kills the crew.", win_vessel=reward, chance_to_win=1.0)
        event.options.append(option)
        # Option 3
        reward = Vessel(health=0, fuel=-2, morale=0, money=-100)
        option = Option(text="Pay a local guide $100 to show you a shortcut around it.",
                        win_text="The guide shows you a better route around it. (-2 fuel, -$100)",
                        win_vessel=reward, chance_to_win=1.0)
        event.options.append(option)
        # Add Event
        events.append(event)
        # endregion

        # region Space-Blockade
        event = Event("Space pirates have setup a blockade in front of you!")

        # Option 1
        reward = Vessel(health=0, fuel=-10, morale=0, money=0)
        option = Option(text="Fly around it.",
                        win_text="You fly all the way around it. (-10 fuel)", win_vessel=reward,
                        chance_to_win=1.0)
        event.options.append(option)
        # Option 2
        reward = Vessel(health=-10, fuel=0, morale=0, money=0)
        option = Option(text="Fire your lasers at them!",
                        win_text="They fire back, but you make it through the blockade. (-10 health)",
                        win_vessel=reward, chance_to_win=1.0)
        event.options.append(option)
        # Option 3
        reward = Vessel(health=0, fuel=0, morale=-2, money=-50)
        option = Option(text="Pay the toll of $50.",
                        win_text="You bribe the pirates to get through. (-2 morale, -$50)",
                        win_vessel=reward, chance_to_win=1.0)
        event.options.append(option)
        # Add Event
        events.append(event)
        # endregion

        # region Space-Disease
        event = Event("You all get Jupiter-Fever. The doc says he can come up with a cure if you sacrifice one "
                      "of the crew to conduct medical research on.", image_path="Images\\Disease.png")

        # Option 1
        reward = Vessel(health=0, fuel=0, morale=-15, money=0)
        consequence = Vessel(health=0, fuel=0, morale=-1000, money=0)
        option = Option(text="The good of the many outweighs the good of the few, or the one.",
                        win_text="The crew draws straws to see who dies. (-15 morale)", win_vessel=reward,
                        fail_text="The crew nominates you to be experimented on.", fail_vessel=consequence,
                        chance_to_win=0.7)
        event.options.append(option)
        # Option 2
        reward = Vessel(health=-20, fuel=0, morale=0, money=0)
        option = Option(text="That's awful, I would never do that.",
                        win_text="Everyone is very sick for weeks. (-20 health)",
                        win_vessel=reward, chance_to_win=1.0)
        event.options.append(option)
        # Add Event
        events.append(event)
        # endregion

        # region Bad-Luck-IRS
        event = Event("Your accountant discovers you owe the IRS $500.", image_path="Images\\IRS.png")

        # Option 1
        reward = Vessel(health=0, fuel=0, morale=0, money=-500)
        option = Option(text="Pay the money.",
                        win_text="You pay the IRS $500", win_vessel=reward,
                        chance_to_win=1.0)
        event.options.append(option)
        # Option 2
        reward = Vessel(health=0, fuel=0, morale=0, money=0)
        consequence = Vessel(health=0, fuel=0, morale=0, money=-1000)
        option = Option(text="Tell them you already paid that money (lie).",
                        win_text="They believe you.", win_vessel=reward,
                        fail_text="They discover that you are lying to them and double your amount owed. (-$1000)",
                        fail_vessel=consequence,
                        chance_to_win=0.45)
        event.options.append(option)
        # Add Event
        events.append(event)
        # endregion

        return events
