import tkinter as tk
import tkinter.font as tkfont
from tkinter import *
from tkinter import messagebox
from tkinter.ttk import *

from PIL import ImageTk, Image
from ttkthemes import ThemedTk

from Missions import *

CONST_THEME_NAME = 'black'
CONST_DEFAULT_IMAGE = 'Images\\StaticSpaceship.png'

# Handles the main logic for the game, and the main UI display
class Game:
    def __init__(self, window=None):

        # setup variables
        # setup default image...
        self.image_raw = Image.open(CONST_DEFAULT_IMAGE)
        self.image_resized = self.image_raw.resize((400, 200), Image.NEAREST)
        self.image = ImageTk.PhotoImage(self.image_resized)
        self.window = window
        self.vessel = Vessel(health=25, fuel=25, morale=25, money=1000)
        self.option_buttons = []

        # Generate the ui elements
        self.generate_ui()

        # Set Default Window Size
        self.window.geometry("1080x920")

        # Set window name
        window.title("STAR-BOY")

        # Set background Color
        self.window.configure(background='#424242')

        # Set some weights
        self.window.grid_columnconfigure(1, weight=1)

        self.events = []

        # generate events...
        self.generate_events()

        self.selected_event = -1

        self.next_choice()

    # apply the player's choice to the state of the spaceship
    def apply_option(self, option):
        self.vessel.apply_change(option.get_ship())

        # add events...
        if option.get_mission() is not None:
            self.events[self.selected_event + 1: self.selected_event + 1] = option.get_mission()

        self.update_ui()
        self.display_result(option)

    # Display the result of the player's choice
    def display_result(self, option):
        # clear buttons
        for button in self.option_buttons:
            button.destroy()

        self.option_buttons.clear()

        # create funny font
        font_style = tkfont.Font(family="Comic Sans MS", size=14)

        # Set label to consequences of choice
        self.question_label.config(text=option.get_final_text())

        # Add continue button
        button = tk.Button(self.option_frame, text=option.text, bg="#424242", fg='white', font=font_style)
        button["text"] = "Continue..."
        button["command"] = lambda: self.next_choice()
        button.grid(row=0, column=0, sticky="ew", pady=20)
        self.option_buttons.append(button)

    # Iterate to the next item in the event list
    def next_choice(self):

        # decrement fuel...
        self.vessel.set_fuel(self.vessel.get_fuel() - 1)

        # clear buttons
        for button in self.option_buttons:
            button.destroy()

        self.option_buttons.clear()
        self.selected_event = self.selected_event + 1

        # create funny font
        font_style = tkfont.Font(family="Comic Sans MS", size=14)

        # append buttons
        row = 0
        event = self.events[self.selected_event]

        # set image if available, otherwise set default
        if event.image_path:
            self.set_image(event.image_path)
        else:
            self.set_image(CONST_DEFAULT_IMAGE)

        self.question_label.config(text=event.text)
        for option in event.options:
            button = tk.Button(self.option_frame, text=option.text, bg="#424242", fg='white', font=font_style)
            button["text"] = option.text
            button["command"] = lambda option=option: self.apply_option(option)
            button.grid(row=row, column=1, sticky="ew", pady=5)
            self.option_buttons.append(button)
            row = row + 1

        # Game over?
        self.check_game_over()

    # Checks for a game over condition
    def check_game_over(self):
        if len(self.events) <= self.selected_event:
            messagebox.showinfo(title="Victory!", message="You won!")
            self.window.destroy()

        if self.vessel.get_health() <= 0:
            messagebox.showinfo(title="Game Over!", message="Everyone is dead and it's your fault.\nGame Over!")
            self.window.destroy()

        elif self.vessel.get_fuel() <= 0:
            messagebox.showinfo(title="Game Over!", message="You run out of fuel and die alone in deep space.\nGame "
                                                            "Over!")
            self.window.destroy()

        elif self.vessel.get_morale() <= 0:
            messagebox.showinfo(title="Game Over!", message="Your crew gets fed up with your sub-par leadership and "
                                                            "mutinies! You are ejected out the airlock.\nGame "
                                                            "Over!")
            self.window.destroy()

        elif self.vessel.Money <= 0:
            messagebox.showinfo(title="Game Over!", message="You have gone bankrupt, but you still owe on your "
                                                            "student loans.\nGame "
                                                            "Over!")
            self.window.destroy()

    # returns a list of all of our events
    def generate_events(self):
        self.events.extend(Missions.one_off_events())
        self.events.extend(Missions.distress_call())
        self.events.extend(Missions.ship_fire())

        # Randomize events
        random.shuffle(self.events)

    # Generates the main user-interface features
    def generate_ui(self):

        # Frame for displaying ship status
        ship_status = Frame(self.window)
        ship_status.pack(side=TOP, pady=50)

        # set health bar
        self.health_bar = Progressbar(ship_status, orient=HORIZONTAL, style="red.Horizontal.TProgressbar", length=500,
                                      mode='determinate')
        self.health_bar['value'] = self.vessel.get_health()
        self.health_bar.grid(row=0, column=1, sticky="we", pady=10, padx=50)

        # set health bar label
        self.health_bar_label = Label(ship_status, text="HEALTH")
        self.health_bar_label.grid(row=0, column=0, sticky="e", pady=10)

        # set fuel bar
        self.fuel_bar = Progressbar(ship_status, orient=HORIZONTAL, style="red.Horizontal.TProgressbar", length=500,
                                    mode='determinate')
        self.fuel_bar['value'] = self.vessel.get_fuel()
        self.fuel_bar.grid(row=1, column=1, sticky="we", pady=10, padx=50)

        # set fuel bar label
        self.fuel_bar_label = Label(ship_status, text="FUEL")
        self.fuel_bar_label.grid(row=1, column=0, sticky="e", pady=10)

        # set morale bar
        self.morale_bar = Progressbar(ship_status, orient=HORIZONTAL, style="red.Horizontal.TProgressbar", length=500,
                                      mode='determinate')
        self.morale_bar['value'] = self.vessel.get_morale()
        self.morale_bar.grid(row=2, column=1, sticky="we", pady=10, padx=50)

        # set morale bar label
        self.morale_bar_label = Label(ship_status, text="MORALE")
        self.morale_bar_label.grid(row=2, column=0, sticky="e", pady=10)

        # create font styles
        font_style_heading = tkfont.Font(family="Comic Sans MS", size=16)
        font_style = tkfont.Font(family="Comic Sans MS", size=11)

        # set money label
        self.money_row_label = Label(ship_status, text="MONEY")
        self.money_row_label.grid(row=3, column=0, stick="e", pady=10)
        self.money_label = Label(ship_status, text="$" + str(self.vessel.Money), font=font_style_heading,
                                 foreground='#a4f542')
        self.money_label.grid(row=3, column=1)

        # Image Frame
        image_frame = Frame(self.window)
        image_frame.pack(side=TOP, pady=50)

        # set image box
        self.image_canvas = Canvas(image_frame, width=400, height=200, bg="#424242")
        self.image_canvas.pack(side=TOP)
        self.image_on_canvas = self.image_canvas.create_image(0, 0, anchor=NW, image=self.image)

        # Question Label
        self.question_label = Label(self.window, text="Question Box", font=font_style_heading, wraplength=800)
        self.question_label.pack(side=TOP, pady=10)

        # Options box
        self.option_frame = Frame(self.window)
        self.option_frame.pack(side=TOP, pady=50)

    # Sets the image to display on screen
    def set_image(self, path):
        self.image_raw = Image.open(path)
        self.image_resized = self.image_raw.resize((400, 200), Image.NEAREST)
        self.image = ImageTk.PhotoImage(self.image_resized)
        self.image_canvas.itemconfig(self.image_on_canvas, image=self.image)

    # Called after a user has selected an option and the consequences have been applied
    def option_done(self):
        # update UI
        self.update_ui()
        # clear option buttons
        self.option_buttons.clear()

    # Updates the UI to ensure that the display is correct after changes made to vessel
    def update_ui(self):
        self.health_bar['value'] = self.vessel.get_health()
        self.fuel_bar['value'] = self.vessel.get_fuel()
        self.morale_bar['value'] = self.vessel.get_morale()
        self.money_label.config(text="$" + str(self.vessel.Money))


# UI Logic
root = ThemedTk(theme=CONST_THEME_NAME)
game = Game(window=root)
game.window.mainloop()
