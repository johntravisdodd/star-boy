# Star-Boy
Star-Boy is a short comedic space-adventure game created with Python using the Tkinter UI library. Star-Boy takes inspiration from Oregon Trail, and flash-based adventure games. 
In Star-Boy players are presented with a series of scenarios where they must pick from a range of options which sometimes hold unintended consequences. If they run out of fuel or any other of their limited resources they will lose, but if they manage to help their crew survive all the wacky space scenarios thrown at them then they win the game. 
Star-Boy is a very short game with about a 5 minute play-time in total. The game may be extended in future by adding more scenarios.

![A scenario presented in Star-Boy.](https://i.imgur.com/mMFlFIN.png)

#
*Created By John Dodd - 10-25-2020*
