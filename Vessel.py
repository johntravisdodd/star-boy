# A vessel to hold health, morale, fuel and money status of the player.
class Vessel:

    def __init__(self, health, morale, fuel, money):
        self._Health = health
        self._Morale = morale
        self._Fuel = fuel
        self.Money = money

    # Merges a another vessel's state with this vessel's state.
    def apply_change(self, vessel):
        self.set_health(vessel.get_health() + self.get_health())
        self.set_fuel(vessel.get_fuel() + self.get_fuel())
        self.set_morale(vessel.get_morale() + self.get_morale())
        self.Money = self.Money + vessel.Money

    # Returns the health of this vessel.
    def get_health(self):
        return self._Health

    # Sets the health of this vessel.
    def set_health(self, value):
        value = min(100, value)
        self._Health = value

    # Gets the morale of this vessel.
    def get_morale(self):
        return self._Morale

    # Sets the morale of this vessel.
    def set_morale(self, value):
        value = min(100, value)
        self._Morale = value

        # Gets the fuel of this vessel.

    def get_fuel(self):
        return self._Fuel

    # Sets the fuel of this vessel.
    def set_fuel(self, value):
        value = min(100, value)
        self._Fuel = value
